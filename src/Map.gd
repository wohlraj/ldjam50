extends Node2D
export(PackedScene) var Tree
export(PackedScene) var CutTree
export(PackedScene) var Colonist
export(PackedScene) var Boat

signal cut_tree
var screen_size
var screen_tile_size
enum Cell {OCEAN, GROUND, COAST}
export(float, 0 , 1) var ocean_percent_max := 0.5
export var ntrees = 20

# Variables create when the scene is ready
onready var tile_map  = $TileMap
var ground_tiles_index 
var ocean_tiles_index
var sand_tiles_index
onready var _inner_tiles = []
onready var current_ntrees = ntrees

var _rng = RandomNumberGenerator.new()

func _ready():
	var tile_set = tile_map.get_tileset()
	ground_tiles_index = tile_set.find_tile_by_name("ground")
	ocean_tiles_index = tile_set.find_tile_by_name("ocean")
	sand_tiles_index = tile_set.find_tile_by_name("sand")
	
	screen_size = get_viewport_rect().size
	screen_tile_size = Vector2(round(screen_size.x / 32), round(screen_size.y / 32))
	_rng.randomize()
	generate()



func generate_random_position():
	var random = RandomNumberGenerator.new()
	random.randomize()
	var x = random.randi_range(0, screen_size.x)
	var y = random.randi_range(0, screen_size.y)
	return Vector2(x, y)

func generate_random_outside_position():
	var random = RandomNumberGenerator.new()
	random.randomize()
	var x = 0
	var y = 0
	var sides = [['side', 'left'], ['side', 'right'], ['vertical', 'top'], ['vertical', 'bottom']]
	var side = sides[random.randi_range(0, 3)]
	if side.has('side'):
		y = random.randi_range(0, screen_size.y)
		if side.has('left'):
			x = -200
		else:
			x = screen_size.x + 200
	else:
		x = random.randi_range(0, screen_size.x)
		if side.has('top'):
			y = -100
		else:
			y = screen_size.y + 100
	return Vector2(x, y)

func spawn_tree(tree):
	add_child(tree)

func spawn_colonist(boat):
	var colonist = Colonist.instance()
	colonist.boat_id = boat.get_instance_id()
	colonist.connect('colonist_return', boat, '_on_colonist_return')
	colonist.connect('tree_cut', self, 'handle_tree_cut')
	add_child(colonist)

func spawn_boat(position):
	var boat = Boat.instance()
	boat.position = position
	boat.connect("collided", self, "handle_boat_collided")
	add_child(boat)

func handle_boat_collided(boat):
	spawn_colonist(boat)

func handle_tree_cut(tree):
	# Edge case where tree might have been deleted already
	if is_instance_valid(tree):
		var cut_tree = CutTree.instance()
		cut_tree.position = tree.position
		add_child(cut_tree)
		tree.queue_free()
	current_ntrees -= 1
	emit_signal("cut_tree", current_ntrees)
	if current_ntrees <= 0:
		get_tree().paused = true
		$EndMenu/LastedValue.text = str($HUD.score)
		$EndMenu.visible = true

func _on_BoatTimer_timeout():
	var random_position = generate_random_outside_position()
	print("Spawning a boat in "+str(random_position))
	spawn_boat(random_position)

func generate():
	generate_ocean_perimeter()
	generate_coast_perimeter()
	generate_inner()
	clean_map_generate()
	generate_trees()

func generate_ocean_perimeter():
	
	for x in [1, screen_tile_size.x - 1]:
		for y in range(1, screen_tile_size.y):
			tile_map.set_cell(x, y, ocean_tiles_index)
			
	for x in range(1, screen_tile_size.x - 1):
		for y in [1, screen_tile_size.y - 1 ]:
			tile_map.set_cell(x, y, ocean_tiles_index)

func generate_coast_perimeter():
	var nbrTileOceanTotal = ( screen_tile_size.x * screen_tile_size.y) * ocean_percent_max
	var tile_ocean = 0
	var rayon = 0
	var rayonMax = screen_tile_size.x/2
	var island_pattern = 0.99

	while tile_ocean < nbrTileOceanTotal :
		for x in [rayon, screen_tile_size.x - rayon]:
			for y in range(rayon, screen_tile_size.y):
				if _rng.randf() < island_pattern && tile_ocean < nbrTileOceanTotal:
					tile_map.set_cell(x, y, ocean_tiles_index)
					tile_ocean += 1
		if tile_ocean < nbrTileOceanTotal:
			for x in range(rayon, screen_tile_size.x - rayon):
				for y in [rayon, screen_tile_size.y - rayon ]:
					if _rng.randf() < island_pattern && tile_ocean < nbrTileOceanTotal:
						tile_map.set_cell(x, y, ocean_tiles_index)
						tile_ocean += 1
		rayon += 1
		island_pattern -= 0.2
		if rayon == rayonMax: break

func generate_inner():
	for x in range(1, screen_tile_size.x - 1):
		for y in range(1, screen_tile_size.y -1):
			if tile_map.get_cell(x, y) == tile_map.INVALID_CELL:
				tile_map.set_cell(x, y, ground_tiles_index)
				var cell_texture = 0
				tile_map.set_cell(x, y, cell_texture)
				_inner_tiles.append(Vector2(x, y))

func clean_map_generate():
	var island_map = tile_map.get_used_cells_by_id(ground_tiles_index)

	for coord_tile in island_map:
		var is_ocean_up = (tile_map.get_cell(coord_tile.x, coord_tile.y + 1) == tile_map.INVALID_CELL || tile_map.get_cell(coord_tile.x, coord_tile.y  + 1) ==  ocean_tiles_index)
		var is_ocean_down = (tile_map.get_cell(coord_tile.x, coord_tile.y - 1) == tile_map.INVALID_CELL || tile_map.get_cell(coord_tile.x, coord_tile.y - 1) ==  ocean_tiles_index)
		var is_ocean_right = (tile_map.get_cell(coord_tile.x + 1, coord_tile.y) == tile_map.INVALID_CELL || tile_map.get_cell(coord_tile.x + 1, coord_tile.y) ==  ocean_tiles_index )
		var is_ocean_left = (tile_map.get_cell(coord_tile.x - 1, coord_tile.y) == tile_map.INVALID_CELL || tile_map.get_cell(coord_tile.x - 1, coord_tile.y) ==  ocean_tiles_index)

		if ( is_ocean_up  && is_ocean_down && is_ocean_right && is_ocean_left ):
		 tile_map.set_cell(coord_tile.x, coord_tile.y, ocean_tiles_index)

		if( is_ocean_up  || is_ocean_down || is_ocean_right || is_ocean_left):
			tile_map.set_cell(coord_tile.x, coord_tile.y, sand_tiles_index)
			
func generate_trees():
	var tree_offset = 40
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var cell_size_x = $TileMap.cell_size.x
	var cell_size_y = $TileMap.cell_size.y
	for _k in range(ntrees):
		var tree = Tree.instance()
		var cell = _inner_tiles[rng.randi() % _inner_tiles.size()]
		tree.position = $TileMap.map_to_world(cell) + Vector2(rng.randi_range(0, cell_size_x), rng.randi_range(0, cell_size_y) - tree_offset)
		spawn_tree(tree)
