extends Node

#export(PackedScene) var AttractionZone
var AttractionZone = preload("res://src/AttractionZone.tscn")

signal mouse_click
export var score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	var dynamic_font = DynamicFont.new()
	dynamic_font.font_data = load("res://assets/font/Hand Drawn Shapes.ttf")
	dynamic_font.size = 64
	$Label.set("custom_fonts/font", dynamic_font)
	dynamic_font.size = 30
	$TreeSaved_label.set("custom_fonts/font", dynamic_font)
	$TimePassed_label.set("custom_fonts/font", dynamic_font)

func _input(event):
	if event is InputEventMouseButton and event.is_pressed() and not event.is_echo():
		emit_signal("mouse_click", event)

func _on_Sit_In_pressed():
	var mouseEventposition = yield(self,"mouse_click").position
	var attractionZone = AttractionZone.instance()
	attractionZone.init("SitIn")
	
	attractionZone.position = mouseEventposition
	add_child(attractionZone)
	

func _on_Fete_pressed():
	var mouseEvent = yield(self,"mouse_click")
	var attractionZone = AttractionZone.instance()
	attractionZone.init("Fete")
	attractionZone.position = mouseEvent.position
	add_child(attractionZone)


func _on_Festival_pressed():
	var mouseEvent = yield(self,"mouse_click")
	var attractionZone = AttractionZone.instance()
	attractionZone.init("Festival")
	attractionZone.position = mouseEvent.position
	add_child(attractionZone)

func _on_HUD_mouse_click(event):
	pass
	
func _on_TreeTimer_timeout():
	pass

func _on_TimerPassed_timeout():
	score += 1
	$TimePassed_label.text = str("Time : ", score)
	
func _on_Map_cut_tree(current_ntrees):
	print(current_ntrees)
	$TreeSaved_label.text = str("Trees : ", current_ntrees)
	
