extends KinematicBody2D

signal colonist_return
signal tree_cut
export var boat_id: int

export var MAX_SPEED = 100
export(int) var SPEED = MAX_SPEED
var target_tree
var target_position
var boat
var timer

enum State {SPAWNED, CUTTING, LEAVING}
export var state = State.SPAWNED

func _ready():
	boat = instance_from_id(boat_id)
	position = boat.position
	print("Colonist starting at boat position: " + str(boat.position))
	# once a colonist has spawned, it moves to the closest tree
	_go_towards_nearest_tree()
	print("Computed target_tree for colonist: ", target_tree.position)

func _go_towards_nearest_tree():
	var trees = get_tree().get_nodes_in_group("trees")
	var nearest_tree = trees[0]
	for tree in trees:
		if tree.position.distance_to(position) < nearest_tree.position.distance_to(position):
			if tree.owner_id == 0: # tree is free
				nearest_tree = tree
	target_tree = nearest_tree
	target_position = nearest_tree.position

func _process(delta):
	if state == State.SPAWNED:
		if !is_instance_valid(target_tree) or target_tree.owner_id != 0:
			_go_towards_nearest_tree()
		if target_tree.position.distance_to(position) <= 10:
			target_tree.owner_id = get_instance_id()
			state = State.CUTTING
			target_position = position
			timer = Timer.new()
			timer.set_wait_time(2)
			timer.connect("timeout", self, "_on_TreeBeingCutDown_timeout")
			add_child(timer) #to process
			timer.start() #to start
	if state == State.LEAVING:
		if boat.position.distance_to(position) <= 10:
			emit_signal('colonist_return')
			print('Colonist removed', self)
			queue_free()

	# Let us move towards our target position
	position += (target_position - position).normalized() * SPEED * delta


func _on_TreeBeingCutDown_timeout():
	state = State.LEAVING
	timer.stop()
	# cut tree
	emit_signal('tree_cut', target_tree)
	# back to the boat
	print("Going back to the boat, vector is: ", boat.position - position)
	target_position = boat.position
