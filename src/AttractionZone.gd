extends Node2D

const Colonist = preload("res://src/Colonist.gd")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func init(type):
	var _typeAttractionZone = type

func _on_AttractionZone_body_entered(body):
	if body is Colonist:
		body.SPEED = body.MAX_SPEED / 2

func _on_AttractionZone_body_exited(body):
	if body is Colonist:
		body.SPEED = body.MAX_SPEED
