extends KinematicBody2D

signal collided
export var n_colonists = 1
var curr_n_colonists
var velocity = Vector2()

enum State {SPAWNED, ARRIVED, LEAVING, EXITED}
var state = State.SPAWNED

func _ready():
	var screen_size = get_viewport_rect().size
	var map_center = Vector2(screen_size.x / 2, screen_size.y / 2)
	velocity = (map_center - position)
	curr_n_colonists = n_colonists

func _physics_process(delta):
	if state == State.SPAWNED:
		var collision = move_and_collide(velocity * delta)
		if collision:
			state = State.ARRIVED
			emit_signal('collided', self)
			curr_n_colonists = 0
	if state == State.LEAVING:
		move_and_slide(-10 * velocity * delta)

func _on_colonist_return():
	curr_n_colonists += 1
	if curr_n_colonists >= n_colonists:
		if state == State.EXITED:
			# We are already out of map, let's delete
			print('Boat removed', self)
			queue_free()
		else:
			state = State.LEAVING

func _on_VisibilityNotifier2D_screen_exited():
	# We delete only if leaving, we might exit early because of physics
	if state == State.LEAVING:
		print('Boat removed', self)
		queue_free()
	else:
		state = State.EXITED
