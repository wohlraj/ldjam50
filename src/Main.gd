extends Node
	
func _on_NewGame_pressed():
	get_tree().change_scene("res://src/Map.tscn")

func _on_StopMusic_pressed():
	Music.stop()

func _on_Quit_pressed():
	get_tree().quit()
